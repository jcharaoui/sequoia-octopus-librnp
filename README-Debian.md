# Debian stable installation

## General

On debian bullseye it is not possible to build versions >= v1.2.1. This is because cargo is shipped in version 0.47 by debian stable. You will get the following error when trying to build:

`this version of Cargo is older than the 2021 edition, and only supports 2015 and 2018 editions.`

The build of <= v1.1.0 is possible, but not compatible with thunderbird 91. You will not be able to receive your keys from the keychain in thunderbird.

The recommended solution is to use a Debian `bullseye` container with a version of `cargo` and `rustc` provided by the [rustup](https://rustup.rs/) third-party installer.

## Preparation

Install the LXC container package on your system:

    sudo apt install lxc

For information about various LXC-specific options, see the [LXC Debian wiki page](https://wiki.debian.org/LXC).

## Container setup

Create a new Debian container with the command below. This will create a new LXC container with the requisite dependencies.

    sudo lxc-create --name sequoia -t debian -- -r bullseye --packages=build-essential,clang,curl,git,libssl-dev,nettle-dev,pkg-config

Now boot the container and attach to its shell:

    sudo lxc-start sequoia
    sudo lxc-attach --name=librnp -- bash

The next step is to run the [rustup](https://rustup.rs/) installer, which will install the latest version of Rust inside the container:

    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

Type `1` to proceed with option `Proceed with installation (default)`.

When the installation is finished, load the environment parameters into the current shell:

    source "$HOME/.cargo/env"

## Build

Now we will build `sequoia-octopus-librnp` in our container environment:

    git clone https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp.git
    cd sequoia-octopus-librnp
    cargo build --release

The build process will download further dependencies from the network and compile the library. This can take a few minutes.

When it is done, the compiled library should appear in the `target/release` directory.

Now you may exit the container shell (using `exit` or `CTRL-D` keys) and shut it down using `sudo lxc-stop sequoia`.

## Install

From the host system, we'll now copy the library file to `/usr/lib/thunderbird` and install the dpkg diversion.

These instructions are adapted from the [main README](https://gitlab.com/sequoia-pgp/sequoia-octopus-librnp#using-debians-thunderbird).

     sudo dpkg-divert --divert /usr/lib/thunderbird/librnp-orig.so --rename /usr/lib/thunderbird/librnp.so
     sudo cp /var/lib/lxc/sequoia/rootfs/sequoia-octopus-librnp/target/release/libsequoia_octopus_librnp.so /usr/lib/thunderbird/
     sudo ln -sf libsequoia_octopus_librnp.so /usr/lib/thunderbird/librnp.so

## Use

Restart Thunderbird to load the new library. The GnuPG keys should be loaded in the Thunderbird OpenPGP manager.
